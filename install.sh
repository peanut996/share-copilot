#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
START_SCRIPT_PATH="/usr/local/bin/scop"
cat > "$START_SCRIPT_PATH" <<EOF
#!/bin/bash
EXECUTABLE_DIR="$SCRIPT_DIR"
EXECUTABLE="\$EXECUTABLE_DIR/share-copilot"
case "\$1" in
  r)
    echo "Running share-copilot..."
    "\$EXECUTABLE"
    ;;
  rb)
    echo "Running share-copilot in the background..."
    "\$EXECUTABLE" >/dev/null 2>&1 &
    ;;
  st)
    echo "Stopping share-copilot..."
    pkill -f "\$EXECUTABLE"
    ;;
  v)
    if pgrep -f "\$EXECUTABLE" >/dev/null; then
      echo "share-copilot is running"
    else
      echo "share-copilot is not running"
    fi
    ;;
  *)
    echo "Usage: \$0 {r|rb|st|v}"
    exit 1
    ;;
esac



exit 0
EOF
chmod +x "$START_SCRIPT_PATH"
echo "-----------------
install success！
-----------------
scop r  --运行
scop rb --后台运行
scop st --停止
scop v  --查看状态
-----------------"
