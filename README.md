# share-copilot 

```
测试环境:Linux4.18.0-305.3.1.el8.x86_64 GNU/Linux
```
### 1.安装

```
git clone https://gitlab.com/luyoyu/share-copilot.git
```
```
cd share-copilot
```
```
bash install.sh
```

### 2.配置

##### 2.1 config.json文件说明

```js
domain//绑定域名

host //ip

port //端口 80或443

certPath//公钥路径-443必须

keyPath//私钥路径-443必须

github_api_url//默认不改

token//自己的副驾驶token,可放多个，请求时随机使用
//登录插件后在%userprofile%\AppData\Local\github-copilot\host.json     ghu_开头

verification//自定义验证
```

##### 2.2 示例 

```json
{
	"server": {
		"domain": "example.com",
		"host": "0.0.0.0",
		"port": 443,
		"certPath":"./example.pem",
		"keyPath":"./example.pem"
	},
	"copilot_config":{
		"github_api_url": "https://api.github.com/copilot_internal/v2/token",
		"token":[
			"yours_token_1",
            "yours_token_2",
            "yours_token_3"
		]},
	"verification":""
}

```
格式不能错，不确定的去这里验证 ->https://www.json.cn/

### 3.启动 

```
scop r  --运行
scop rb --后台运行
scop st --停止
scop v  --查看状态
```

### 4.完整示例一：

config.json

```json
{
	"server": {
		"domain": "api.example.com",
		"host": "0.0.0.0",
		"port": 443,
		"certPath":"./example.pem",
		"keyPath":"./example.pem"
	},
	"copilot_config":{
		"github_api_url": "https://api.github.com/copilot_internal/v2/token",
		"token":[
		"ghu_xMNAYLcJAPqAfiGoobrWffkJoNcGMVJtETKA",
        "ghu_GZgKFwraHorAxXXUvsUclOhxiYERPsSJeNuF",
        "ghu_SPUTCLvkMKoeMstPJmhSlYsYvCojhkFjGubl"
		]},
	"verification":"i_am_free"
}
```

运行截图

![demo.png](https://img1.imgtp.com/2023/09/09/jNTqxhR8.png)

时间-合计请求次数: 0 | 成功次数 0 | 失败次数 0   统计次数在00:00:00归零

修改%userprofile%\AppData\Local\github-copilot\host.json

```json
{
    "github.com":{
        "user":"suibian",
        "oauth_token":"i_am_free",
        "dev_override":{
            "copilot_token_url":"https://api.example.com/copilot_internal/v2/token"
        }
    }
}
```
测试： 

![demo2.png](https://img1.imgtp.com/2023/09/09/FHDNLixL.png)
